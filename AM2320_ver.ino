#include "application.h"
#include "AM2320.h"
#include "si_iot.h"
#include "si_a2d_float.h"
#include "psychro.h"

#define PIN_VAISALA_TEMP A1
#define PIN_VAISALA_RH A2
#define PIN_AM1_I2C_EN D7
#define PIN_AM2_I2C_EN D6
#define PIN_AM3_I2C_EN D5
#define PIN_AM4_I2C_EN D4

char web_string[600];
int i = 0;

AM2320 sensor;
si_iot_data AM1_rh("AM1_rh", SI_IOT_FLOAT);
si_iot_data AM1_t("AM1_t", SI_IOT_FLOAT);
si_iot_data AM2_rh("AM2_rh", SI_IOT_FLOAT);
si_iot_data AM2_t("AM2_t", SI_IOT_FLOAT);
si_iot_data AM3_rh("AM3_rh", SI_IOT_FLOAT);
si_iot_data AM3_t("AM3_t", SI_IOT_FLOAT);
si_iot_data AM4_rh("AM4_rh", SI_IOT_FLOAT);
si_iot_data AM4_t("AM4_t", SI_IOT_FLOAT);
si_iot_data V_rh("V_rh", SI_IOT_FLOAT);
si_iot_data V_t("V_t", SI_IOT_FLOAT);
si_iot_data i2c_er("i2c_er", SI_IOT_INT);

//prototypes
float viasala_rh();
float viasala_temp();

void setup(void)
{
  Serial.begin(9600);
  Wire.begin();

  //configure pins
  pinMode(PIN_AM1_I2C_EN, OUTPUT);
  digitalWrite(PIN_AM1_I2C_EN, LOW);
  pinMode(PIN_AM2_I2C_EN, OUTPUT);
  digitalWrite(PIN_AM2_I2C_EN, LOW);
  pinMode(PIN_AM3_I2C_EN, OUTPUT);
  digitalWrite(PIN_AM3_I2C_EN, LOW);
  pinMode(PIN_AM4_I2C_EN, OUTPUT);
  digitalWrite(PIN_AM4_I2C_EN, LOW);

  //preload for testing
  AM1_rh.set((float)100.1);
  AM1_t.set((float)100.2);
  V_rh.set((float)100.3);
  V_t.set((float)100.4);

  Particle.variable("data", web_string);
}

void loop(void)
{

  digitalWrite(PIN_AM1_I2C_EN, HIGH);
  if(sensor.read_sensor_readings() != 0) i2c_er.set(i2c_er.geti() + 1);
  AM1_rh.set(sensor.getHumidity());
  AM1_t.set(c_to_f(sensor.getTemperature()));
  digitalWrite(PIN_AM1_I2C_EN, LOW);

  digitalWrite(PIN_AM2_I2C_EN, HIGH);
  if(sensor.read_sensor_readings() != 0) i2c_er.set(i2c_er.geti() + 1);
  AM2_rh.set(sensor.getHumidity());
  AM2_t.set(c_to_f(sensor.getTemperature()));
  digitalWrite(PIN_AM2_I2C_EN, LOW);

  digitalWrite(PIN_AM3_I2C_EN, HIGH);
  if(sensor.read_sensor_readings() != 0) i2c_er.set(i2c_er.geti() + 1);
  AM3_rh.set(sensor.getHumidity());
  AM3_t.set(c_to_f(sensor.getTemperature()));
  digitalWrite(PIN_AM3_I2C_EN, LOW);

  digitalWrite(PIN_AM4_I2C_EN, HIGH);
  if(sensor.read_sensor_readings() != 0) i2c_er.set(i2c_er.geti() + 1);
  AM4_rh.set(sensor.getHumidity());
  AM4_t.set(c_to_f(sensor.getTemperature()));
  digitalWrite(PIN_AM4_I2C_EN, LOW);

  V_rh.set(viasala_rh());
  V_t.set(viasala_temp());
  for(i = 0; i < 600; i++) web_string[i] = 0; //clear char array
  i2c_er.json_cat(web_string, 0);
  AM1_rh.json_cat(web_string, 1);
  AM1_t.json_cat(web_string, 1);
  AM2_rh.json_cat(web_string, 1);
  AM2_t.json_cat(web_string, 1);
  AM3_rh.json_cat(web_string, 1);
  AM3_t.json_cat(web_string, 1);
  AM4_rh.json_cat(web_string, 1);
  AM4_t.json_cat(web_string, 1);
  V_rh.json_cat(web_string, 1);
  V_t.json_cat(web_string, 2);
  Serial.println(web_string);
  delay(5000);
}

float viasala_rh()
{
  float rh;
  int a2d_val;
  a2d_val = analogRead(PIN_VAISALA_RH);
  rh = (((float)a2d_val / (float)4095) * (float)330) + .3;
  return rh;
}

float viasala_temp()
{
  float temp;
  int a2d_val;
  a2d_val = analogRead(PIN_VAISALA_TEMP);
  temp = (((float)a2d_val / (float)4095) * (float)660) + 0.7;
  return temp;
}
